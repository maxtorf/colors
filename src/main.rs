fn main() {
    ok_blue("Hello, World!");
}

fn ok_blue(text: &str) {
    print!("\x1b[0;31m{}\n", text);
}
